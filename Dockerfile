FROM mcr.microsoft.com/dotnet/sdk:5.0

RUN apt update && \
 apt install -y build-essential python3-dev virtualenv && \
 cd && virtualenv .jupyter_venv -p python3

ENV VIRTUAL_ENV=/root/.jupyter_venv
ENV PATH=$VIRTUAL_ENV/bin:$PATH:/root/.dotnet/tools

RUN pip3 install pep517 && \
 pip3 install jupyter && \
 pip3 install jupyterlab && \
 dotnet tool install -g microsoft.dotnet-interactive && \
 dotnet interactive jupyter install

RUN if [ $(uname -m) = x86_64 ] ; then \
 cd && wget https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB && \
 apt-key add GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB && rm  GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB && \
 echo deb https://apt.repos.intel.com/mkl all main > /etc/apt/sources.list.d/intel-mkl.list && \
 apt-get update && apt-get install -y intel-mkl-64bit-2020.4-912 && \
 ldconfig $(find /opt -name "libiomp5.so" | sed 's/\/libiomp5.so$//') ; fi

RUN jupyter server --generate-config && \
 echo "c.ServerApp.password = u'argon2:\$argon2id\$v=19\$m=10240,t=10,p=8\$sDh++k0SuBMoEU6lHgYdgg\$Bxfjmx/ytT88ipgVZi0Ihg'" >> ~/.jupyter/jupyter_server_config.py && \
 echo "c.ServerApp.allow_origin = '*'" >> ~/.jupyter/jupyter_server_config.py && \
 mkdir ~/.notebooks

EXPOSE 8888
ENTRYPOINT [ "jupyter", "lab", "--ip=0.0.0.0", "--port=8888", "--allow-root", "--no-browser", "--notebook-dir=~/.notebooks" ]
