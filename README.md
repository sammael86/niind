# .NET Interactive Jupyter Notebooks in Docker

Run:

`docker compose up -d`

Open in browser:

`http://localhost:8888`

Password is empty.
